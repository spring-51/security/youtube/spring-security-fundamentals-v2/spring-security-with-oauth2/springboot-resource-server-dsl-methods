package com.replaceme.server.resource.config;

import com.replaceme.server.resource.constant.MyConstants;
import net.minidev.json.JSONArray;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.stream.Collectors;

@Configuration
public class MyResourceServerConfig extends WebSecurityConfigurerAdapter {

    @Value(value = "${my.resourceserver.public.key: invalidKey}")
    private String publicKey;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // TODO
        /*
        this config is taking symmetric key ,
        how can we configure it for asymmetric key
         */
        http.oauth2ResourceServer(
                c -> c.jwt(
                        j -> j.decoder(jwtDecoder())
                                /*
                                Spring sec internally foes not take GrantedAUthority from "authority" key of
                                jwt. IN order to put GrantedAuthority in Authentication obj we added below
                                config
                                 */
                                .jwtAuthenticationConverter(jwtAuthenticationConverter())
                )
        );

        http.authorizeRequests()
                .mvcMatchers(MyConstants.HELLO_EP+"/**").hasAnyAuthority("read","write")
                .anyRequest().authenticated();
    }

    @Bean
    public JwtDecoder jwtDecoder(){
        // Config for Symmetric key - start
        // WORKING

        String key = "123456789012345678901234567890123456";
        SecretKey secretKey = new SecretKeySpec(key.getBytes(), 0 ,key.getBytes().length,"AES");
        return NimbusJwtDecoder.withSecretKey(secretKey).build();

        // Config for Symmetric key - end

        // Config for Asymmetric key - start
        // FIXME
        /*
        String key = publicKey;
        SecretKey secretKey = new SecretKeySpec(key.getBytes(), 0 ,key.getBytes().length,"AES");
        return NimbusJwtDecoder.withPublicKey(secretKey).build();

         */
        // Config for Asymmetric key - end

    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(){
        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter((jwt -> {
            /*
            here "authorities" is the key in json of jwt which gives us information
            to create GrantedAuthority and place it in Authentication
             */
            JSONArray authorities = (JSONArray)jwt.getClaims().get("authorities");
            return authorities.stream()
                    .map(String :: valueOf)
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
        }));
        return converter;
    }
}
