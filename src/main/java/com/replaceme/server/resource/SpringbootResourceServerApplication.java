package com.replaceme.server.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootResourceServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootResourceServerApplication.class, args);
	}

}
