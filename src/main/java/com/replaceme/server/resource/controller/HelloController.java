package com.replaceme.server.resource.controller;

import com.replaceme.server.resource.constant.MyConstants;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = MyConstants.HELLO_EP)
public class HelloController {

    @GetMapping
    public String hello() {
        return "hello from resource server : HelloController -> hello() ";
    }
}
